var Tail = require('always-tail');
var fs = require('fs');
const { Client } = require('node-osc');
const express = require('express')
const app = express()
const port = 3000

let shouldEmitOsv = false;

app.get('/start', (req, res) => {
    shouldEmitOsv = true;
    console.log('Starting to send events!')
    res.send('Starting to send events!')
})
app.get('/stop', (req, res)=> {
    shouldEmitOsv = false;
    console.log('Stop sending events!')
    res.send('Stop sending events!')
})
app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`))

// 152 - prod
// 17 - cg
// 36 - self
const client = new Client('10.254.1.17', 12321);

var filename = "C:/Users/LEDGEN5/AppData/LocalLow/Wizards Of The Coast/MTGA/output_log.txt";

if (!fs.existsSync(filename)) fs.writeFileSync(filename, "");

var tail = new Tail(filename, '\n');

let activePlayer = 0;

tail.on('line', function(data) {
    try {
        const test = JSON.parse(data);

        const messageBlocks = test.greToClientEvent.greToClientMessages;

        const relevantBlocks = messageBlocks.filter(block => {
            return block.type = 'GREMessageType_GameStateMessage' && typeof block.gameStateMessage !== 'undefined';
        })

        relevantBlocks.map(block => {
            const turnInfo = block.gameStateMessage.turnInfo;

            if(activePlayer !== turnInfo.activePlayer) {
                activePlayer = turnInfo.activePlayer;

                if(shouldEmitOsv) {
                    switch(activePlayer) {
                        case 1: 
                            client.send('/press/bank/1/9', () => {
                                console.log('Player 1 playing');
                            })
                            break;
                        
    
                        case 2: 
                            client.send('/press/bank/1/10', () => {
                                console.log('Player 2 playing');
                            });
                            break;
                        
                    }
                }

                
            }

            
        })
    }
    catch(e) {
        // console.log('error')
    }
});


tail.on('error', function(data) {
  console.log("error:", data);
});

tail.watch();
